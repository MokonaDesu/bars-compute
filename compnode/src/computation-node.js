$(function () {
    var urlBase = 'http://localhost:27015';
    var statusMsgControl = $('#status');
    var tasksFinished = 0;

    function setStatusMessage(msg) {
	statusMsgControl.html(msg);
    }

    function performRequest(method, url, data, callback) {
	$.ajax({
	    type: method,
	    url: url,
	    data: data,
	    contentType: "application/json",
	    success: function(json) {
		if (callback) {
		    callback(json);
		}
	    },
	    error: function(request, status, msg) {
		setStatusMessage('Something went wrong =( Please reload the page. The error was: ' + msg);
		console.log(request);
		console.log(status);
		console.log(msg);
	    }
	});
    }
    
    function processTask(task) {
	var algorithm = new Function("data", task.code);
	
	var result = {
	    id: task.id,
	    result:  algorithm(JSON.parse(task.data))
	};

	return result;
    }

    function processAndPost(data) {
	var taskResult = processTask(data);

	setStatusMessage('Posting back the results...');
	performRequest('POST', urlBase + '/postResult', JSON.stringify(taskResult), function(data) {
	    tasksFinished++;
	    setStatusMessage('Querying for another task... (Tasks done in this session: ' + tasksFinished + ')');
	    setTimeout(requestNewTask);
	});
    }
    
    function requestNewTask() {
	performRequest('GET', urlBase + '/getTask', null, function(data) {
	    setStatusMessage('Processing the task...');
	    processAndPost(data);
	});
    };

    
    setStatusMessage('Querying for the first task...');
    requestNewTask();
});
