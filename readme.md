## BARS_COMPUTE ##

#### S.C. (Service Consumer): ??? ####
* Calls the S.N. and submits tasks unto '/postTask'

#### C.N. (Computation Node): ####
* Get task via HTTP GET '/getTask'
* Process the S.N. response (Json result)
* Execute the task from Json result
* HTTP POST the task result unto '/postResult'
 
#### S.N. (Submittion Node): ####
* Manage C.N and S.C's connections
* Manage C.N and Task Queue
* Split tasks into parts and give them to C.N's

Task format:
The S.C. submitted task has to have the following format:
  
    {
      code: 'function(item) { return item * 2; }'
      data: [1, 2, 3]
    }
    
The C.N. recieves tasks in the same format, but with an 'id' field and has to post back object of the following structure:
  
    {
      result: [2, 4, 6],
      id: '<same as recieved from S.N.>'
    }
