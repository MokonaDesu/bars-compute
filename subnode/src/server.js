var http = require('http');
var url = require('url');
var querystring = require('querystring');

var Task = require('./task').Task;
var util = require('./util');

var connectedClients = [];
var tasks = [];

function buildResponseObject(jsonpCallback, responseData) {
    if (jsonpCallback) {
	return jsonpCallback + '(' + JSON.stringify(responseData) + ')';
    } else {
	return JSON.stringify(responseData);
    }
}

function updateQueue() {
    if (tasks.length > 0 && connectedClients.length > 0) {
	var task = util.first(tasks, function(task) { return task.status == 'new'; });
	if (task) {
	    var client = connectedClients.shift();
	    util.logInfo('Task started (id=' + task.id + ';remoteAddr=' + client.req.connection.remoteAddress + ')');
	    task.status = 'started';
	    client.res.end(buildResponseObject(client.callback, task.task));
	}
    }
}

function processWorkerRequest(commsInfo) {
    connectedClients.push(commsInfo);
    util.logInfo('New client in queue (remoteAddr=' + commsInfo.req.connection.remoteAddress + ';jsonpCallback=' + commsInfo.callback + ')');
    updateQueue();
}

function enqueueTask(commsInfo, taskData) {
    var task = new Task(taskData);
    util.logInfo('New task in queue (id=' + task.id + ';jsonpCallback=' + commsInfo.callback + ')');
    tasks.push({ id: task.id, task: task, status: 'new', commsInfo: commsInfo });
    updateQueue();
}

function closeTask(commsInfo, taskData) {
    taskData = JSON.parse(taskData);

    var targetTask = util.first(tasks, function(task) { return task.id == taskData.id; });
    if (targetTask) {
	targetTask.commsInfo.res.end(buildResponseObject(targetTask.callback, taskData.result));
	commsInfo.res.end(JSON.stringify({result: 'success'}));
	util.remove(tasks, targetTask);
	util.logInfo('Done processing task (id=' + targetTask.id + ')');
    } else {
	util.logWarning('Computation node ' + commsInfo.req.connection.remoteAddress + ' attempted to submit result for non-existant task.');
    }
}

function recievePostData(commsInfo, doneCallback) {
    var postContents = '';

    if (commsInfo.req.method == 'OPTIONS') {
	util.logInfo('Answered an OPTIONS request from ' + commsInfo.req.connection.remoteAddress);
	commsInfo.res.end();
	return;
    }

    if (commsInfo.req.method != 'POST') {
	util.logWarning('Invalid method (expected POST and got ' + commsInfo.req.method + ') detected when processing request from ' + commsInfo.req.connection.remoteAddress + '. Answered with HTTP 405.');
        commsInfo.res.writeHead(405);
        commsInfo.res.end();
	return;
    }
    
    commsInfo.req.on('data', function(chunk) {
        postContents += chunk.toString(); 
    });
    
    commsInfo.req.on('end', function() {
	doneCallback(postContents);
    });
}

function processTaskPost(commsInfo) {
    recievePostData(commsInfo, function(data) {
	enqueueTask(commsInfo, data);
    });
}

function processResultPost(commsInfo) {
    recievePostData(commsInfo, function(data) {
	closeTask(commsInfo, data);
    });
}

function buildCommsInfoObj(req, res) {
    var requestUrl = url.parse(req.url);
    var queryParams = querystring.parse(requestUrl.query);
    return {
	req: req,
	res: res,
	callback: queryParams.callback,
	endpoint: requestUrl.pathname,
	isJsonpClient: function() { return !!this.callback; }
    };
}

function writeResponseHead(commsInfo) {
    if (commsInfo.isJsonpClient())
	commsInfo.res.writeHead(200, {
	    'Content-Type': 'application/json'
	});
    else
	commsInfo.res.writeHead(200, {
	    'Content-Type': 'application/json',
	    'Access-Control-Allow-Origin': '*',
	    'Access-Control-Allow-Methods': 'GET, POST',
	    'Access-Control-Allow-Headers': 'Content-Type, Accept',
	    'Access-Control-Max-Age': '1000'
	});
}

function processRequest(req, res) {
    try {
	var commsInfo = buildCommsInfoObj(req, res);
	writeResponseHead(commsInfo);

	switch(commsInfo.endpoint) {
        case '/getTask':
            processWorkerRequest(commsInfo);
            break;
        case '/postTask':
            processTaskPost(commsInfo);
            break;
        case '/postResult':
            processResultPost(commsInfo);
	    break;
	default:
	    util.logWarning('Non-existant endpoint accessed by ' + req.connection.remoteAddress);
	}
    }
 
    catch(e) {
	util.logError(e.toString());
    }
}

var host = process.argv[2] || '127.0.0.1';
var port = parseInt(process.argv[3]) || 27015;

var server = http.createServer(processRequest);
['SIGINT', 'SIGTERM'].forEach(function(exitSignal) {
    process.on(exitSignal, function() {
	server.close();
	connectedClients.forEach(function(client) {
	    client.req.connection.destroy();
	});
	tasks.forEach(function(task) {
	    task.commsInfo.req.connection.destroy();
	});
	process.stdout.cursorTo(0);
	util.logInfo('Bye...');
    });
});

util.logInfo('Server running @ ' + host + ':' + port);
server.listen(port, host);
