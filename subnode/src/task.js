var globalTaskId = 0;

module.exports.Task = function(taskInitializer) {
    var task = JSON.parse(taskInitializer);
    
    this.id = globalTaskId++;
    this.data = task.data;
    this.code = task.code;
    
    if (!this.validateCode()) {
        throw new Error('Processing algorhithm is invalid.');
    }
}

//TODO: Code validation
module.exports.Task.prototype.validateCode = function() {
    return true;
}