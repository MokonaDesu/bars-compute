var colors = require('colors');

module.exports.first = function(list, predicate) {
    var len = list.length;
    for (var index = 0; index < len; index++) {
	var item = list[index];
	if (predicate(item, index)) {
	    return item;
	}
    }

    return null;
}

module.exports.remove = function(list, item) {
    var index = list.indexOf(item);
    if (index != -1) {
	list.splice(index, 1);
    }
}

module.exports.logWarning = function(warningText) {
    console.log(colors.yellowBG(colors.black('[WARN]')) + ' ' + new Date().toLocaleTimeString() + ' ' + warningText); 
}

module.exports.logInfo = function(infoText) {
    console.log(colors.whiteBG(colors.black('[INFO]')) + ' ' + new Date().toLocaleTimeString() + ' ' + infoText);
}

module.exports.logError = function(errorText) {
    console.log(colors.redBG(colors.black('[ERR!]')) + ' ' + new Date().toLocaleTimeString() + ' ' + errorText);
}
